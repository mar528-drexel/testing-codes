<!DOCTYPE html>
<html> <!-- This code was written by Michael Raphael for CS 164-062-->
<style> 
#buttonContainer
{
	width: 500px;
	height: 100px;
}
#heartContainer
{
	width: 500px;
	height: 55.5px;
}
#container 
{
	width: 500px;
	height: 500px;
	border: 1px solid black;
}
#tileOne, #tileFive
{
	background-color: #transparent;
	width: 200px;
	height: 50px;
	float: left;
}
#tileTwo, #tileFour
{
	background-color: #transparent;
	width: 50px;
	height: 100px;
	float: left;
}
#tileThree
{
	background-color: #transparent;
	width: 100px;
	height: 100px;
	float: left;
	margin-top: 0;
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
}
#box
{
	background-image: url("https://i.imgur.com/D0yqfkb.gif");
	width:200px;
    height:200px;
	border: 1px solid transparent;
}
table, th, td
{
	border: 1px solid black;
	padding: 10px;
	border-collapse: collapse;
	margin: 10px;
}
#heartSix, #heartSeven, #heartEight, #heartNine
{
	background-image: url("https://i.imgur.com/YhUcfbV.png");
	height: 55.5px;
	width: 55.5px;
	float: left;
}
#heartOne, #heartTwo, #heartThree, #heartFour, #heartFive
{
	background-image: url("https://i.imgur.com/9xvcGAb.png");
	height: 55.5px;
	width: 55.5px;
	float: left;
}


h1{ margin: 0; }
#manualButton { 
	margin: 25px;
	width: 200px; 
	height: 50px; 
	line-height: 50px;
	color: black;
	text-align: center;
	cursor: pointer;
	border: 1px solid black;
}
#manualButton:hover { background: #ffe1e6; }

#snailManual { 
  display: none;

  border: 5px solid black; 
  padding: 35px;
  width: 800px;
  text-align: left;
  background: #fff;	
  position: fixed;
  top:50%;
  left:50%;
  transform: translate(-50%,-50%);
  -webkit-transform: translate(-50%,-50%) 
}
</style>
<div id = "container" onmousemove = "showLiveCoordinates(event)"> 
	<div id = "box">
		<div id = "tileOne" onmouseover = "mouseHoverTileOne()"></div>
		<div id = "tileTwo" onmouseover = "mouseHoverTileTwo()" onclick = "selectRandomItem()"></div>
		<p id = "tileThree" onmouseover = "mouseHoverTileThree()" onmousedown = "mouseHoldingDown()" onmouseup = "mouseRelease()"></p>
		<div id = "tileFour" onmouseover = "mouseHoverTileFour()" onclick = "eatRandomItem()"></div>
		<div id = "tileFive" onmouseover = "mouseHoverTileFive()"></div>
	</div>
</div>
<div id = "heartContainer">
	<div id = "heartOne"></div>
	<div id = "heartTwo"></div>
	<div id = "heartThree"></div>
	<div id = "heartFour"></div>
	<div id = "heartFive"></div>
	<div id = "heartSix"></div>
	<div id = "heartSeven"></div>
	<div id = "heartEight"></div>
	<div id = "heartNine"></div>
</div>
<div id = "buttonContainer">
<center>
<div id="manualButton">Click here for snail manual!</div>
<div id="snailManual">
	<center><h1>Snail Manual</h1>
	<small>The tutorial and information for the virtual gastropod mollusk made with Javascript by Michael Raphael. <a href="https://bitbucket.org/mar528-drexel/testing-codes/raw/5d100fb8b7dfe20ceb1ea78fb2023bb33ecdb499/Pet.js" target="_blank">Here</a> is a link to the code with markups on what everything does. <a href="http://www.pages.drexel.edu/~mar528/StateMachinePage.html" target="_blank">Here</a> is a link to the state machine. <a href="http://www.snail-world.com/" target="_blank">Here</a> is a link to snail fun facts.  </small></center>
	<p><b>*IMPORTANT NOTE: If you cannot see the full manual, adjust the zoom of the page. You can zoom out by holding (ctrl and - ) and zoom in by holding (ctrl and +).</b></p>
	<p><img src="https://i.imgur.com/TrmPfOF.gif" width = "150" height = "150" align= "left">
	There are different interactions for when you hover your cursor on the snail. If your cursor is a bit above the snail their eyes widen in excitement to meet their new friend! If the cursor is a bit to the left of the snail their shell opens like a drawer, revealing refrigerated beverages. If the cursor is a bit to the right of the snail they open their mouth, looking so hungry it is almost as if they are willing to eat anything! Lastly, if the cursor is a bit to the bottom of the snail, a tentacle comes out of their shell, trying to shoo you away from their refrigerator, that is not very nice of them.
	</p>
	<p><img src="https://i.imgur.com/XdwFgD6.gif" width = "150" height = "150" align= "right">
	So you look that tentacle straight in the eyes, yes those were eyes, did you think they were suction cups? Anyways, you look that tentacle straight in the eyes and you take his beverages by clicking close to the left of the snail when the refrigerator is opening! Be careful when you reach in because there is not only soda, it seems there is also dynamite, swords, skateboards, paint buckets, and miniature snails, all of which you can feed to the snail by click close to the right of them. Whether or not the snail will enjoy the held item is common sense. Dynamite and swords are painful, paint buckets and miniature snails are always a 50/50, and sodas and skateboards are delicious.  
	</p>
	<p>
		<img src="https://i.imgur.com/vLieIHZ.gif" width = "75" height = "75" align = "left">
		<img src="https://i.imgur.com/YifRynD.gif" width = "75" height = "75" align = "left">
		<img src="https://i.imgur.com/D0yqfkb.gif" width = "75" height = "75" align = "left">
		<img src="https://i.imgur.com/bQz4oSS.gif" width = "75" height = "75" align = "left">
		<img src="https://i.imgur.com/tEM1Lid.gif" width = "75" height = "75" align = "left">What they eat affects their mood. Their mood is neutral by default (middle animation), but if they keep eating delicious things they begin to smile (2nd to the right animation), and if they are lucky enough to keep the streak going they fall in love with you, hearts constantly floating at their side (furthest right animation). If they keep eating gross things they begin to cry (2nd to the left animation), and if they are unlucky enough to keep the streak going, what you have put in their stomach creates an excruciating amount of pain, launching them into cycles of rapid spasms (furthest left animation). The amount of hearts below the snail also indicates their current emotional state. 1 heart = rapid spasms. 2-3 hearts = crying. 4-6 hearts = neutral. 7-8 hearts = smiling. 9 hearts = hearts fluttering.
	</p>
	<p><img src="https://i.imgur.com/fLeEroP.gif" width = "150" height = "150" align= "right">If you want to pick up the snail and move them around feel free to do so. You can do this by moving the cursor to the center of the snail, then click and holding down the mouse button, and drag them wherever you please. Do keep in mind though that the snail can only move within the play area, which is the area defined by the black border.
	</p>
	<p><img src="https://i.imgur.com/pd7aHdB.gif" width = "150" height = "150" align= "left">If your cursor is not close enough to the snail, do not worry! You can still interact with them as long as your cursor stays inside the play area. Even after moving the snail all over the place, they will still find your cursor. Try it yourself, he is an observant one! The snail looks in the direction of the cursor location relative to themself. The animations could possibly be the snail looking up left, up, up right, right, down right, down, down left, and left. 
	</p> 
	<p>Creators note: I chose a snail as my virtual pet because they are cute. The different states the snail can be in are based on what they are fed, which is completely random. I felt this adds an aspect of goofiness and fun for interacting with the pet. I also created it so there is no visible buttons during the interaction and changing the state of the snail is a 2 step process, obtaining an item and feeding them. I did this because I want the user to feel more involved with the state change of the pet rather than having the state instantly change with 1 button. Lastly, I created the art for the pet myself, 19 unique animations, and 8 pictures, totalling 66 unique drawn images. The documentation for the pet is linked at the top of the manual and state machine.
	</p>
</div>
</center>
</div>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="http://www.pages.drexel.edu/~mar528/jquery-ui.js"></script>
<script>
document.title = "Snail";
var boxWidth = box.offsetWidth, boxHeight = box.offsetHeight, state = 0, item = 0, itemPicture = 0, happinessLevel = 5, oneHeartFlag = 0; //These are where SOME variables are initialized. An important thing to note here is that the default variable happinessLevel(measure of how happy the snail is) is equal to 5, it is in the middle of the possible snail happinessLevel values that are between 1 - 9 inclusive to start the snail in the most neautral state.

$(function()
{
	$('#manualButton').click(function()//THIS IS FOR THE BUTTON THAT MAKES THE MANUAL POPUP
	{
		$('#snailManual').fadeToggle();
	})
	$(document).mouseup(function (e)
	{
		var container = $("#snailManual");
		if (!container.is(e.target) 
        && container.has(e.target).length === 0) 
    {
        container.fadeOut();
    }});
	{
	$( "#box" ).draggable({ containment: "#container", handle: "p", cursorAt: { top: 100, left: 100 } }); //This line essentially establishes the box containing the image of the snail as draggable. It also establishes the boundary of where the snail can't be moved past, which is the outer box with a black border called the "container". It also makes it so when the snail is being dragged, it centers the image to the mouse location, this helps maintain the holding animation because the snail sometimes lags behind the mouse if it moves too fast, 'causing the snail to switch to a different animation while still being dragged around. To get the minimum possibility of this occuring, I centered the image with the mouse location when dragged. Lastly, it makes it so the snail could only be dragged by the <p> element. 
	$( "div, p" ).disableSelection(); //This in conjunction of the previous line of code makes it so I cannot drag from from <div>, only <p>.
	}
});
function selectRandomItem() //When the user clicks on the left portion of the snail, this is the function that is called. It essentially can only be clicked if the snail is not currently in a transitioning animation (state == 0) and if there is no item being currently held (item == 0). If both are true, It generates a random number between 1 - 6 inclusively ( variable itemPicture), because there are 6 if statements, each corresponding to a unique item picture to change the cursor image to. Although there are 6 unique item pictures, the value( variable item) of each item can only be 1 or 2. When the itemPicture is between 1 - 2 inclusive, the value equals 1. When the itemPicture is between 3 - 4 inclusive, the value is randomly generated, and can either be 1 or 2, 50/50 chance. When the itemPicture is between 5 - 6, the value is equal to 2.
{	
	if ((state == 0) && (item == 0))
	{
		itemPicture = Math.floor(Math.random() * 7);
		if(itemPicture == 1)
		{
			item = 1;
			$("body").css('cursor','url(https://i.imgur.com/WebBp98.png), auto'); //changes mouse cursor to SODA CAN PICTURE
		}
		if(itemPicture == 2)
		{
			item = 1;
			$("body").css('cursor','url(https://i.imgur.com/dLVREc7.png), auto');//changes mouse cursor to SKATEBOARD PICTURE
		}
		if(itemPicture == 3)
		{
			item = Math.floor(Math.random()*(2-1+1)+1);
			$("body").css('cursor','url(https://i.imgur.com/Sta0dFn.png), auto');//changes mouse cursor to PAINT BUCKET PICTURE
		}
		if(itemPicture == 4)
		{
			item = Math.floor(Math.random()*(2-1+1)+1);
			$("body").css('cursor','url(https://i.imgur.com/yPKNe5s.png), auto');//changes mouse cursor to MINI SNAIL PICTURE
		}
		if(itemPicture == 5)
		{
			item = 2;
			$("body").css('cursor','url(https://i.imgur.com/AfkGnE9.png), auto');//changes mouse cursor to SWORD PICTURE
		}
		if(itemPicture == 6)
		{
			item = 2;
			$("body").css('cursor','url(https://i.imgur.com/XNpHmxM.png), auto');//changes mouse cursor to DYNAMITE PICTURE
		}
	}
}
function eatRandomItem()//This function is called only when the right portion of the snail is clicked with an item being held. It sets the variable state equal to 1, this means that the snail is locked into a transitioning animation. The animation could either be the snail spazzing while chewing, or the snail comfortably chewing, which animation the snail displays is based on the value of the item. If the item value equals 1, the snail shows the comfortably chewing animation (HAPPY ANIMATION), then after 3000 milliseconds returns the heartAnimation function. If the item value equals 2, the snail shows the spazzing while chewing animation (SAD ANIMATION), then after 3000 milliseconds returns the cryingAnimation function. 
{
	if(item == 1)
	{
		state = 1;
		$("body").css('cursor','default');//changes cursor back to default
		item = 0;
		document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/mZj4EDY.gif')"; //HAPPY ANIMATION 1
		if((happinessLevel >= 1) && (happinessLevel < 9))// If between this range happinessLevel increases by 1.
		{
			happinessLevel++;
		}
		if((happinessLevel == 2)) //THESE IF STATEMENTS BELOW ARE THE HEART REFORMING ANIMATION
		{
			document.getElementById("heartTwo").style.backgroundImage = "url('https://i.imgur.com/qvenymW.gif')";
		}
		if( happinessLevel == 3)
		{
			document.getElementById("heartThree").style.backgroundImage = "url('https://i.imgur.com/qvenymW.gif')";
		}
		if( happinessLevel == 4)
		{
			document.getElementById("heartFour").style.backgroundImage = "url('https://i.imgur.com/qvenymW.gif')";
		}
		if( happinessLevel == 5)
		{
			document.getElementById("heartFive").style.backgroundImage = "url('https://i.imgur.com/qvenymW.gif')";
		}
		if( happinessLevel == 6)
		{
			document.getElementById("heartSix").style.backgroundImage = "url('https://i.imgur.com/qvenymW.gif')";
		}
		if( happinessLevel == 7)
		{
			document.getElementById("heartSeven").style.backgroundImage = "url('https://i.imgur.com/qvenymW.gif')";
		}
		if( happinessLevel == 8)
		{
			document.getElementById("heartEight").style.backgroundImage = "url('https://i.imgur.com/qvenymW.gif')";
		}
		if( happinessLevel == 9)
		{
			document.getElementById("heartNine").style.backgroundImage = "url('https://i.imgur.com/qvenymW.gif')";
		}
		return setTimeout(heartAnimation, 3000); 
	}
	if(item == 2)
	{
		state = 1;
		$("body").css('cursor','default'); //changes cursor back to default
		item = 0;
		document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/vLieIHZ.gif')"; //SAD ANIMATION 1
		if((happinessLevel <= 9) && (happinessLevel > 1))
		{
			happinessLevel--;
		}
		if((happinessLevel == 1)) //THESE IF STATEMENTS BELOW ARE THE HEART BREAKING ANIMATION
		{
			document.getElementById("heartTwo").style.backgroundImage = "url('https://i.imgur.com/JkTSn97.gif')";
		}
		if( happinessLevel == 2)
		{
			document.getElementById("heartThree").style.backgroundImage = "url('https://i.imgur.com/JkTSn97.gif')";
		}
		if( happinessLevel == 3)
		{
			document.getElementById("heartFour").style.backgroundImage = "url('https://i.imgur.com/JkTSn97.gif')";
		}
		if( happinessLevel == 4)
		{
			document.getElementById("heartFive").style.backgroundImage = "url('https://i.imgur.com/JkTSn97.gif')";
		}
		if( happinessLevel == 5)
		{
			document.getElementById("heartSix").style.backgroundImage = "url('https://i.imgur.com/JkTSn97.gif')";
		}
		if( happinessLevel == 6)
		{
			document.getElementById("heartSeven").style.backgroundImage = "url('https://i.imgur.com/JkTSn97.gif')";
		}
		if( happinessLevel == 7)
		{
			document.getElementById("heartEight").style.backgroundImage = "url('https://i.imgur.com/JkTSn97.gif')";
		}
		if( happinessLevel == 8)
		{
		document.getElementById("heartNine").style.backgroundImage = "url('https://i.imgur.com/JkTSn97.gif')";
		}
	return setTimeout(cryingAnimation, 3000); 
	}
}
function cryingAnimation()//Animation for when the item makes the snail sad. This animation lasts 3000 milliseconds, then the changeStateToZero function is returned. Also changes the default animation of the snail depending on their happinessLevel.
{
	document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/YifRynD.gif')"; //SAD ANIMATION 2
	if( happinessLevel == 1)
	{
		document.getElementById("heartTwo").style.backgroundImage = "url('https://i.imgur.com/YhUcfbV.png')";
	}
	if( happinessLevel == 2)
	{
		document.getElementById("heartThree").style.backgroundImage = "url('https://i.imgur.com/YhUcfbV.png')";
	}
	if( happinessLevel == 3)
	{
		document.getElementById("heartFour").style.backgroundImage = "url('https://i.imgur.com/YhUcfbV.png')";
	}
	if( happinessLevel == 4)
	{
		document.getElementById("heartFive").style.backgroundImage = "url('https://i.imgur.com/YhUcfbV.png')";
	}
	if( happinessLevel == 5)
	{
		document.getElementById("heartSix").style.backgroundImage = "url('https://i.imgur.com/YhUcfbV.png')";
	}
	if( happinessLevel == 6)
	{
		document.getElementById("heartSeven").style.backgroundImage = "url('https://i.imgur.com/YhUcfbV.png')";
	}
	if( happinessLevel == 7)
	{
		document.getElementById("heartEight").style.backgroundImage = "url('https://i.imgur.com/YhUcfbV.png')";
	}
	if( happinessLevel == 8)
	{
		document.getElementById("heartNine").style.backgroundImage = "url('https://i.imgur.com/YhUcfbV.png')";
	}
	return setTimeout(changeStateToZero, 3000); 
}
function heartAnimation()//Animation for when the item makes the snail happy. This animation lasts 3000 milliseconds, then the changeStateToZero function is returned. Also changes the default animation of the snail depending on their happinessLevel.
{
	document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/tEM1Lid.gif')"; //HAPPY ANIMATION 2
	if( happinessLevel == 2)
	{
		document.getElementById("heartTwo").style.backgroundImage = "url('https://i.imgur.com/9xvcGAb.png')";
	}
	if( happinessLevel == 3)
	{
		document.getElementById("heartThree").style.backgroundImage = "url('https://i.imgur.com/9xvcGAb.png')";
	}
	if( happinessLevel == 4)
	{
		document.getElementById("heartFour").style.backgroundImage = "url('https://i.imgur.com/9xvcGAb.png')";
	}
	if( happinessLevel == 5)
	{
		document.getElementById("heartFive").style.backgroundImage = "url('https://i.imgur.com/9xvcGAb.png')";
	}
	if( happinessLevel == 6)
	{
		document.getElementById("heartSix").style.backgroundImage = "url('https://i.imgur.com/9xvcGAb.png')";
	}
	if( happinessLevel == 7)
	{
		document.getElementById("heartSeven").style.backgroundImage = "url('https://i.imgur.com/9xvcGAb.png')";
	}
	if( happinessLevel == 8)
	{
		document.getElementById("heartEight").style.backgroundImage = "url('https://i.imgur.com/9xvcGAb.png')";
	}
	if( happinessLevel == 9)
	{
		document.getElementById("heartNine").style.backgroundImage = "url('https://i.imgur.com/9xvcGAb.png')";
	}
	return setTimeout(changeStateToZero, 3000); 
}
function changeStateToZero()//Changes variable state value equal to 0. This means that the snail is not locked in a transitioning animation. Returns the mouseHoverTileThree function.
{
	state = 0;
	return mouseHoverTileThree();
}
function mouseHoldingDown()//Changes the snail animation to the holding animation (animation when snail is being picked up and dragged around) when the mouse is being held down in the center portion of the snail.
{
	document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/5taLsyF.gif')";
}
function mouseRelease()//When mouse is released from being held, returns the mouseHoverTileThree function.
{
	return mouseHoverTileThree();
}
function showLiveCoordinates(event) //This function constantly updates variables as long as the mouse is within the boundaries of the container (the box containing the snail with the black border). It essentially gets the top left, top right, bottom left, and bottom right coordinates of the snail image picture relative to the top and left of the screen. These coordinates are used for establishing the conditions for unique animations when the mouse is within the container, but not on the snail image.
{
	var liveXCoordinateOfMousePosition = event.clientX; //retrieves live X coordinate.
	var liveYCoordinateOfMousePosition = event.clientY; //retrieves live Y coordinate.
	var boxTopAndLeftOffset = $("#box").offset(); //Allows retrieval of the top and left offset of the box
	topLeftBoxXCoordinate = boxTopAndLeftOffset.left; //Top left box X coordinate.
	topLeftBoxYCoordinate = boxTopAndLeftOffset.top; //Top left box Y coordinate.
	topRightBoxXCoordinate = topLeftBoxXCoordinate + boxWidth; //Top right box X coordinate.
	topRightBoxYCoordinate = topLeftBoxYCoordinate; //Top right box Y coordinate.
	bottomLeftBoxXCoordinate = topLeftBoxXCoordinate; //Bottom left box X coordinate.
	bottomLeftBoxYCoordinate = topLeftBoxYCoordinate + boxHeight; //Bottom left box Y coordinate.
	bottomRightBoxXCoordinate = topLeftBoxXCoordinate + boxWidth; //Bottom right box X coordinate.
	bottomRightBoxYCoordinate = topLeftBoxYCoordinate + boxHeight; //Bottom right box Y coordinate.
	if((liveXCoordinateOfMousePosition < topLeftBoxXCoordinate) && (liveYCoordinateOfMousePosition < topLeftBoxYCoordinate) && (state == 0)) //CONTAINER TILE ONE. If the mouse position is top left relative to the snail image.
	{
		document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/ZYhFaKm.gif')"; //LOOKING TOP LEFT ANIMATION
	}
	if((liveXCoordinateOfMousePosition < topLeftBoxXCoordinate) && (liveYCoordinateOfMousePosition > topLeftBoxYCoordinate) && (liveYCoordinateOfMousePosition < bottomLeftBoxYCoordinate) && (state == 0)) //CONTAINER TILE FOUR. If the mouse position is left relative to the snail picture.
	{
		document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/Yik0esk.gif')"; //LOOKING LEFT ANIMATION
	}
	if((liveXCoordinateOfMousePosition < topLeftBoxXCoordinate) && (liveYCoordinateOfMousePosition > bottomLeftBoxYCoordinate) && (state == 0)) //CONTAINER TILE SEVEN. If the mouse position is bottom left relative to the snail image.
	{
		document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/TFv0Hvk.gif')"; //LOOKING BOTTOM LEFT
	}
	if((liveXCoordinateOfMousePosition > topLeftBoxXCoordinate) && (liveXCoordinateOfMousePosition < topRightBoxXCoordinate) && (liveYCoordinateOfMousePosition < topLeftBoxYCoordinate) && (state == 0)) //CONTAINER TILE TWO. If the mouse position is top relative to the snail image.
	{
		document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/E2t5bXK.gif')"; //LOOKING TOP ANIMATION
	}
	if((liveXCoordinateOfMousePosition > topLeftBoxXCoordinate) && (liveXCoordinateOfMousePosition < topRightBoxXCoordinate) && (liveYCoordinateOfMousePosition > bottomLeftBoxYCoordinate) && (state == 0)) //CONTAINER TILE EIGHT. If the mouse position is bottom relative to the snail image.
	{
		document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/9tzqLp5.gif')"; //LOOKING BOTTOM ANIMATION
	}
	if((liveXCoordinateOfMousePosition > topRightBoxXCoordinate) && (liveYCoordinateOfMousePosition < topLeftBoxYCoordinate) && (state == 0)) //CONTAINER TILE THREE. If the mouse position is top right relative to the snail image.
	{
		document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/DqyOWbb.gif')"; //LOOKING TOP RIGHT ANIMATION
	}
	if((liveXCoordinateOfMousePosition > topRightBoxXCoordinate) && (liveYCoordinateOfMousePosition > topLeftBoxYCoordinate) && (liveYCoordinateOfMousePosition < bottomLeftBoxYCoordinate) && (state == 0)) //CONTAINER TILE SIX. If the mouse position is right relative to the snail image.
	{
		document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/mpQuFg9.gif')"; //LOOKING RIGHT ANIMATION
	}
	if((liveXCoordinateOfMousePosition > topRightBoxXCoordinate) && (liveYCoordinateOfMousePosition > bottomLeftBoxYCoordinate) && (state == 0)) //CONTAINER TILE NINE. If the mouse position is bottom right relative to the snail image.
	{
		document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/KhYrFzL.gif')"; //LOOKING BOTTOM RIGHT ANIMATION
	}
}
function mouseHoverTileOne() //If top portion of the snail image is being hovered, and variable state equals 0.
{
	if (state == 0)
	{
		document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/7OQh8uZ.gif')"; //TOP SNAIL ANIMATION
	}
}
function mouseHoverTileTwo() //If left portion of the snail image is being hovered, and variable state equals 0.
{
	if (state == 0)
	{
		document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/fBCRUe7.gif')"; //LEFT SNAIL ANIMATION
	}
}
function mouseHoverTileThree() //If center portion of the snail image is being hoevered, and the variable state equals 0, display an animation based on the variable happinessLevel value.
{
	if (state == 0)
	{	if(happinessLevel == 1)
		{
			document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/vLieIHZ.gif')"; //EXTREME SADNESS ANIMATION
		}
		if((happinessLevel > 1) && (happinessLevel <= 3))
		{
			document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/YifRynD.gif')"; //CRYING ANIMATION
		}
		if((happinessLevel >= 4) && (happinessLevel <= 6))
		{
			document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/D0yqfkb.gif')"; //NEUTRAL ANIMATION
		}
		if((happinessLevel >= 7) && (happinessLevel < 9))
		{
			document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/bQz4oSS.gif')"; //SMILING ANIMATION
		}
		if(happinessLevel == 9)
		{
			document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/tEM1Lid.gif')"; //EXTREME HAPPINESS ANIMATION
		}
	}
}
function mouseHoverTileFour() //If right portion of the snail image is being hovered, and variable state equals 0.
{
	if (state == 0)
	{
		document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/UoeSMLt.gif')"; //RIGHT SNAIL ANIMATION
	}
}
function mouseHoverTileFive() //If bottom portion of the snail image is being hovered, and variable state equals 0.
{
	if (state == 0)
	{
		document.getElementById("box").style.backgroundImage = "url('https://i.imgur.com/q2XPBoO.gif')"; //BOTTOM SNAIL ANIMATION
	}
}
</script>
</html>